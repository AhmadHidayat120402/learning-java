package com.dicoding.javafundamental.basic;

import com.dicoding.javafundamental.basic.kendaraan.Kereta;
import com.dicoding.javafundamental.basic.kendaraan.Mobil;
import com.dicoding.javafundamental.basic.kendaraan.Motor;
import com.dicoding.javafundamental.basic.musik.Gitar;
import org.apache.commons.lang3.time.DateUtils;

import java.util.Date;
import java.util.Locale;

public class Main {
    public static void main(String [] args){
        System.out.println("dayat");
        Gitar.bunyi();
        Mobil.jumlahBan();
        Motor.jumlahBan();
        Kereta.jumlahBan();

        Date today = new Date();
        System.out.println("Hari Ini : " + today);
        Date tomorrow = DateUtils.addDays(today, 1);
        System.out.println("besok = " + tomorrow);

        char[] dicodingChars = {'d','i','c','o','d','i','n','g'};
        String dicodingString = new String(dicodingChars);
        System.out.println("ini hasil dari char = " + dicodingString);
        int lenghtText = dicodingString.length();
        System.out.println(lenghtText);
        char result = dicodingString.charAt(6);
        System.out.println(result);

        System.out.println("=== String.format ===");
        String name = "Ahmad Hidayat";
        int age = 21;
        double salary = 10000.000;
        String formatedString = String.format("Nama : %s, Usia : %d, Gaji : %.2f", name,age,salary);
        System.out.println(formatedString);

        System.out.println("=== subString() ===");
        String alamat = "probolinggo";
        String ambilAlamat = alamat.substring(1);
        System.out.println(ambilAlamat);

        System.out.println("=== contains ===");
        //contain ini menghasilkan output true dan false
        String namaLengkap = "Ahmad Hidayat";
        String namaAwal = "Ahmad";
        String namaBelakang = "Dayat";

        boolean containsSubstring1 = namaLengkap.contains(namaAwal);
        boolean containSubstring2 = namaLengkap.contains(namaBelakang);

        System.out.println("Nama Lengkap : " + namaLengkap);
        System.out.println("contains : Ahmad = " + containsSubstring1);
        System.out.println("coantain : hidayat = " + containSubstring2);

        System.out.println("=== equals ===");
       // membandingkan apakah dua strng memiliki value yang identik
        String str1 = "hello world";
        String str2 = "hello world";
        String str3 = "hello dayat";

        boolean isEqual1to2 = str1.equals(str2);
        boolean isEqual1to3 = str1.equals(str3);

        System.out.println("str1 is qeual str2 : " + isEqual1to2);
        System.out.println("str1 is equal str3 : " + isEqual1to3);

        System.out.println("=== isEmpthy() ===");
//        method untuk mengecek apakah String itu terdapat value atau kosong

        String namaKota1 = "madiun";
        String namaKota2 = "";

        boolean  nonIsEmpty = namaKota1.isEmpty();
        boolean  isEmpty = namaKota2.isEmpty();

        System.out.println("is empty = " + nonIsEmpty);
        System.out.println("is not empty = " + isEmpty);

        System.out.println("=== concat() ===");
//        method ini digunakan untuk menggabungkan dua string menjadi satu
        String string1 = "hello ";
        String string2 = "world";
        String gabung =  string1.concat(string2);
        System.out.println(gabung);

        System.out.println("=== replace() ===");
//        method yang digunakan untuk mengganti semua value didalam string
        String namaku = "ahmad Hidayat";
        char oldChar = 'a';
        char newChar = 'O';

        String replacaString = namaku.replace(oldChar,newChar);
        System.out.println(replacaString);

        System.out.println("=== indexOf");
//        method yang digunakan untuk mengethui index dari sebuah string
        String namamu = "hello world";
        String namaKamu = "world";

        int indexof = namamu.indexOf(namaKamu);
        System.out.println(indexof);

        System.out.println("=== toLowerCase() ===");
//        method yang digunakan untuk mengubha String menjadi huruf kecil
        String namaIbu = "KHOLIFAH";
        String lowercase = namaIbu.toLowerCase();
        System.out.println(lowercase);

        System.out.println("=== toUppercase() ===");
//        method yang digunakan unutk mengubah String menjadi hurf besar
        String namaAyah = "muhammad yusuf";
        String toUpp = namaAyah.toUpperCase();
        System.out.println(toUpp);

        System.out.println("=== trim() ====");
//        method yang digunakan untuk menghapsu spasi yang ksosng bauk di awla dan diakhir
        String stringwithspace = "   progarammer    ";
        String trimString = stringwithspace.trim();
        System.out.println(trimString);

        System.out.println("===valueOf() ===");
//        method yang digunakan untuk konversi int ke string
        int age1 = 21;
        String format = String.valueOf(age1);
        System.out.println("udah diubah ke String : " + format);

        System.out.println("=== compareTo() ===");
//        method ini dugunakan untuk mmebandingkan dua value apakah sama atau tidak jika sama  aka mengembalikan nilai 0 jika tidak maka mengmebalikan nilai negatif 1 (-1)
        String buah1 = "aple";
        String buah2 = "mangga";
        String buah3 = "aple";

        int result1to2 = buah1.compareTo(buah2);
        int result1to3 = buah1.compareTo(buah3);

        System.out.println(result1to2);
        System.out.println(result1to3);





    }
}
