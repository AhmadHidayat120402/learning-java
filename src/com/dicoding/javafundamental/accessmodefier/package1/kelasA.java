package com.dicoding.javafundamental.accessmodefier.package1;

public class kelasA {
   private int memberA =  5;
   private int functionA(){
       return memberA;
   }

   char memberB = 'A';
   double memberC = 5.6;
   int functionB(){
       int hasil = functionA() + memberA;
       return hasil;
   }
}
