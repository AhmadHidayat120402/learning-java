package com.dicoding.javafundamental.accessmodefier.package3;

public class Perhitungan {
//    public  int nilai =9;
    public static int nilai =9;

//    final int nilaiku = 90;
    static final double Pi = 3.14;

    protected static int getNilai(){
        return nilai;
    }
    Perhitungan(){
        nilai++;
    }
}
