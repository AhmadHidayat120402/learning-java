package com.dicoding.javafundamental.accessmodefier.package3;

public class Main {
    public static void main(String[] args) {
//        Perhitungan hitung = new Perhitungan();
//        System.out.println("nilainya adalah : " + hitung.nilai);

        System.out.println("hitung  ya : " + Perhitungan.nilai);

        for (int i = 0; i < 5; i++) {
            new Perhitungan();
        }

        System.out.println("sampai " + Perhitungan.nilai);
        System.out.println("ambil nilai : " + Perhitungan.getNilai());
        System.out.println("\n nilai dari PI : " + Perhitungan.Pi);
    }
}
