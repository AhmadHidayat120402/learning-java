package com.dicoding.javafundamental.oop.inhertitance;

public class Main {
    public static void main(String[] args) {
        Hewan hewan = new Hewan();
        System.out.println("apakah hewan is-a objek : " + (hewan instanceof Object));
        System.out.println("apakah hewan is-a objek : " + (hewan instanceof Hewan));
        System.out.println("apakah hewan is-a objek : " + (hewan instanceof Kucing));
        System.out.println("=====");

        Kucing kucing = new Kucing();
        System.out.println("apakah kucing is-a objek : " + (kucing instanceof Object));
        System.out.println("apakah kucing is-a objek : " + (hewan instanceof Hewan));
        System.out.println("apakah kucing is-a objek : " + (hewan instanceof Kucing));


    }
}
