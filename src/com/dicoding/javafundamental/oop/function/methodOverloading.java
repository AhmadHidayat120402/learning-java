package com.dicoding.javafundamental.oop.function;

public class methodOverloading {
    public static void main(String[] args) {
        double p = 10;
        double l = 12.9;
        double hasil1 = hitungLuas(p,l);

        System.out.println("luas pertama : " + hasil1);

        System.out.println("====");

        int pa = 10;
        int le = 5;
        int hasil2 = hitungLuas(pa,le);
        System.out.println("luas kedua : " + hasil2);
    }

    public static double hitungLuas(double panjang, double lebar){
        double luas = panjang * lebar;
        return luas;
    }

    public  static  int hitungLuas(int panjang, int lebar){
        int luas = panjang * lebar;
        return  luas;
    }
}
