package com.dicoding.javafundamental.oop;

public class Main {
    public static void main(String[] args) {
//        Hewan hewan = new Hewan();
//        hewan.cetakNama("elang");

        Hewan elang = new Hewan("elang");
        Hewan kucing = new Hewan("kucing");

        elang.beratHewan(2);
        elang.jumlahKakinya(2);
        elang.cetakNama();

        kucing.beratHewan(3);
        kucing.jumlahKakinya(4);
        kucing.cetakNama();

    }
}
