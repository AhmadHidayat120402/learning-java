package com.dicoding.javafundamental.oop;

public class Hewan {

    String name;
    int berat;
    int jumlahKaki;

    public Hewan(String namaHewan){
        name = namaHewan;
    }

    public void beratHewan(int beratnya){
         berat = beratnya;
    }

    public  void jumlahKakinya (int jumlahKakii){
        jumlahKaki = jumlahKakii;
    }

    public  void cetakNama(){
        System.out.println("nama hewan : " + name);
        System.out.println("berat hewan : " + berat);
        System.out.println("banyak kaki : " + jumlahKaki);
    }
//    public void cetakNama(String nama){
//        System.out.println("nama nya : " + nama);
//    }
}
