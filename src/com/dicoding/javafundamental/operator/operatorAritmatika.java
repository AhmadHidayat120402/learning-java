package com.dicoding.javafundamental.operator;

public class operatorAritmatika {
    public static void main(String[] args) {
        System.out.println("operasi penjumlahan");
        int hasilPenjumlahan = 5 +1;
        System.out.println(hasilPenjumlahan);
        System.out.println();

        System.out.println("operasi pengurangan");
        int hasilPengurangan = 4-2;
        System.out.println(hasilPengurangan);
        System.out.println();

        System.out.println("operasi pengalian");
        int hasilPerkalian = 4 * 4;
        System.out.println(hasilPerkalian);
        System.out.println();

        System.out.println("operasi pembagian");
        int hasilPembagian = 20/2;
        System.out.println(hasilPembagian);
        System.out.println();

        System.out.println("operasi modulus");
        int hasilSisa = 9 % 2;
        System.out.println(hasilSisa);

    }
}
