package com.dicoding.javafundamental.operator;

public class operatorUnary {
    public static void main(String[] args) {
        System.out.println("operator unary plus");
        int nilaiAwal = 5;
        int hasil = +nilaiAwal;
        System.out.println(hasil);

        System.out.println("operator unary minus");
        int nilaiAwal2 = 5;
        int hasil2 = -nilaiAwal2;
        System.out.println(hasil2);

        System.out.println("Operator peningkatan 1 poin");
        int nilaiAwal3 = 4;
        nilaiAwal3++;
        System.out.println(nilaiAwal3);

        System.out.println("Operator pengurangan nilai sebesar 1 point");
        int nilaiAwal4 = 5;
        nilaiAwal4--;
        System.out.println("Hasil 5-- = " + nilaiAwal4);
        System.out.println();

        System.out.println("Operator komplemen logika");
        boolean sukses = false; //Nilai sukses adalah false
        System.out.println("Hasil !false = " + !sukses);
        System.out.println();
    }
}
