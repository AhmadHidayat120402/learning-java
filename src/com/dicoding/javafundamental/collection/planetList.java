package com.dicoding.javafundamental.collection;

import java.util.ArrayList;
import java.util.List;

public class planetList {
    public static void main(String[] args) {
//        deklarasi array
        String[] heroes = new String[2];
        heroes[0] = "ahmad";
        heroes[1] = "dayat";

        List<String> planets = new ArrayList<>();
        planets.add("merkurius");
        planets.add("venus");
        planets.add("bumi");
        planets.add("mars");

        System.out.println("list planets awal");
        for (int i = 0; i< planets.size(); i++){
            System.out.println("\t index- " + i + " = " + planets.get(i));
        }
        planets.remove("venus");

        System.out.println("list planets terakhir");
        for (int i = 0; i < planets.size(); i++) {
            System.out.println("\t index- " + " = " + planets.get(i));
        }

        }
    }

