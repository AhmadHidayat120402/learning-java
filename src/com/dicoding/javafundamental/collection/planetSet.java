package com.dicoding.javafundamental.collection;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class planetSet {
    public static void main(String[] args) {
        Set<String> planets = new HashSet<>();
        planets.add("merkurius");
        planets.add("venus");
        planets.add("bumi");
        planets.add("mars");

        System.out.println("set planets awal : (size = " + planets.size() + ")");
        for (String planet : planets){
            System.out.println("\t " + planet);
        }

        planets.remove("venus");

        System.out.println("set terakhir : (size = " + planets.size() + ")");
        for (Iterator iterator = planets.iterator(); iterator.hasNext();){
            System.out.println("\t " + iterator.next());

        }
    }
}
