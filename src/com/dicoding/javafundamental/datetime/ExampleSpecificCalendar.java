package com.dicoding.javafundamental.datetime;

import javax.swing.plaf.synth.SynthOptionPaneUI;
import java.util.Calendar;

public class ExampleSpecificCalendar {
    public static void main(String[] args) {
        Calendar cal = Calendar.getInstance();
        System.out.println("waktu sekarang : " + cal.getTime());

//        menampilkan waktu 15 hari yang lau
        cal.add(Calendar.DATE, -15);
        System.out.println("15 hari yang lalu : " + cal.getTime());

//        menampilkan waktu 4 bulan yang akan datang
        cal.add(Calendar.MONTH, 4);
        System.out.println("4 bulan lagi : " + cal.getTime());

//        menampilkan waktu 2 tahun yang akan adatang
        cal.add(Calendar.YEAR, 2);
        System.out.println("2 tahun lagi : " + cal.getTime());
    }
}
