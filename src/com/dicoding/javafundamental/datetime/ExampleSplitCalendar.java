package com.dicoding.javafundamental.datetime;

import java.util.Calendar;

public class ExampleSplitCalendar {
    public static void main(String[] args) {
//        menampilkan waktu sekarang
        Calendar cal = Calendar.getInstance();
        System.out.println("waktu sekarang : " + cal.getTime());

//        menampilkan waktu spesifik yang diinigankan
        System.out.println("tanggal : " + cal.get(Calendar.DATE));
        System.out.println("bulan : " + cal.get(Calendar.MONTH));
        System.out.println("tahun : " + cal.get(Calendar.YEAR));
    }
}
