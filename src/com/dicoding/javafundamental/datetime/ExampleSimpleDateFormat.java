package com.dicoding.javafundamental.datetime;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ExampleSimpleDateFormat {
    public static void main(String[] args) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String dateFormated = sdf.format(new Date());
        System.out.println("format tgl default : " + new Date());
        System.out.println("format tgl dengan format : " + dateFormated);

//        int a = (int) 3.45;
//        System.out.println(a);

    }
}
