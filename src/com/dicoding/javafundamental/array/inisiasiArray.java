package com.dicoding.javafundamental.array;

public class inisiasiArray {
    public static void main(String[] args) {
        int[] arrInt = new int[]{1,2,3,4,5,6};

        System.out.println(arrInt[0]);
        System.out.println(arrInt[1]);
        System.out.println(arrInt[2]);
        System.out.println(arrInt[3]);
        System.out.println(arrInt[4]);
        System.out.println(arrInt[5]);


        for(int x = 0; x < arrInt.length; x++){
            arrInt[x] = x + 1;
            System.out.println(arrInt[x]);
        }

        System.out.println( "panjangnya = " + arrInt.length);
    }
}
