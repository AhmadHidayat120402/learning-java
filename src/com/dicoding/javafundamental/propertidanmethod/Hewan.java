package com.dicoding.javafundamental.propertidanmethod;

public class Hewan {
    double tinggi = 30.9;
    double lebar = 30.8;
    int umur = 5;

  Hewan(int umur){
        this.umur = umur;
    }

    void lari(){
        System.out.println("berlari");
    }

    void jalan(){
        System.out.println("berjalan");
    }

    void makan(){
        System.out.println("makan");
    }

    double getTinggi(){
      return tinggi;
    }

    double getLebar(){
      return lebar;
    }

    int getUmur(){
      return umur;
    }
}
