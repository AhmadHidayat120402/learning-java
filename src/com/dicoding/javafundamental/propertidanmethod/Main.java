package com.dicoding.javafundamental.propertidanmethod;

public class Main {
    public static void main(String[] args) {
        Hewan kucing = new Hewan(2);

        kucing.jalan();
        kucing.lari();
        kucing.makan();

        System.out.println(kucing.getLebar());
        System.out.println(kucing.getTinggi());
        System.out.println(kucing.getUmur());
    }
}
