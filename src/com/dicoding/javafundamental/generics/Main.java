package com.dicoding.javafundamental.generics;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List lo = new ArrayList<>();
        lo.add("lo - String 1");
        lo.add(new planet("mercury", 0.09));


//        lp.add("lp-string 1");

        for (Object o : lo){
            planet p = (planet) o;
            p.print();
        }


        List<planet> lp = new ArrayList<>();
        lp.add(new planet("bumi", 0.9));
        lp.add(new planet("mars", 0.9));

        for (planet p : lp){
            p.print();
        }

    }
}
