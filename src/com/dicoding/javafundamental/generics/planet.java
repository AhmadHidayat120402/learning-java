package com.dicoding.javafundamental.generics;

public class planet {
    private String name;
    private double mass;

    public planet(String name, double mass){
        this.name = name;
        this.mass = mass;
    }

    public void print(){
        System.out.println("planet : " + name + ", mass : " + mass);
    }
}
